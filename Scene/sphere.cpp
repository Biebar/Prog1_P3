#include "vector.hpp"
#include "color.hpp"
#include "object.hpp"
#include "sphere.hpp"
#include "ray.hpp"
#include <iostream>

using namespace rt;

Sphere::Sphere(point c, double r, color co){
  set_color(co);
  center = c;
  radius = r;
}

point Sphere::get_center() const{
  return center;
}

double Sphere::get_radius() const{
  return radius;
}

vector Sphere::get_normal(const point& intersection) const {
	return (intersection - center).unit();
}

double Sphere::send(ray& r) const{
  vector ac = center - r.get_origin();
  vector u = r.get_direction().unit();
  double ac_u = ac|u;
  double discriminant = 4*(ac_u*ac_u - (ac|ac) + radius*radius);
  if (discriminant > 0)
    return (2*ac_u - sqrt(discriminant)) / 2;
  else
    return -1;
}


hit Sphere::intersect(ray& r) const{
  double d = send(r);
  point intersect = d * r.get_direction() + r.get_origin();
  vector normal = intersect - center;
  color col;
  /* TODO: calcul de col */
  hit h(r, intersect, normal, col);
  return h;

}
