#pragma once

#include <cmath>
#include "object.hpp"
#include "vector.hpp"
#include "ray.hpp"
#include "hit.hpp"

namespace rt{

class Sphere: public Object{

private:
  point center;
  double radius;
public:
  /* Constructor */
  Sphere(point c, double r, color co);

  /* Get methods */

  point get_center() const;
  double get_radius() const;

  vector get_normal(const point& intersection) const;

  /* Returns the distance between the origin of the ray and the intersection of the radius and the sphere */
  double send(ray& r) const;


  hit intersect(ray& r) const;

};
}
