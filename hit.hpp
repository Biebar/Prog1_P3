#pragma once

#include "ray.hpp"
#include "color.hpp"
#include "vector.hpp"

namespace rt {

class hit {
private:
	ray gen;
	vector point,
	       normal;
	color col;
public:
	hit(ray g, vector p, vector n, color c) :
		gen(g),
		point(p),
		normal(n),
		col(c)
	{}

	inline ray get_ray() const {
		return gen;
	}
	
	inline vector get_point() const {
		return point;
	}

	inline vector get_normal() const {
		return normal;
	}

	inline color get_color() const {
		return col;
	}

};
}
