#include "ray.hpp"

using namespace rt;
ray::ray(vector o, vector d, color c)
{
    orig = o;
    dir = d.unit();
    col = c;
}

ray::ray(vector o, vector d)
{
    orig = o;
    dir = d.unit();
    col = color::WHITE;
}

vector ray::get_origin() const
{
    return orig;
}

vector ray::get_direction() const
{
    return dir;
}

color ray::get_color() const
{
    return col;
}
