#pragma once

#include "Screen/color.hpp"
#include "vector.hpp"

namespace rt
{
    class ray
    {

    private:
        vector orig;
        vector dir;
        color col;

    public:
        ray(vector o, vector d, color c);
        ray(vector o, vector d);
        vector get_origin() const;
        vector get_direction() const;
        color get_color() const;
    };
}
