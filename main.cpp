#include <iostream>
#include <unistd.h>
#include <fstream>
#include <cassert>
#include <vector>

#include "image.hpp"
#include "screen.hpp"
#include "color.hpp"
//#include "scene.hpp"
#include "sphere.hpp"
//#include "plane.hpp"
#include "camera.hpp"
#include "vector.hpp"

#define WIDTH 640
#define LENGTH 480

using namespace std;

/**static void draw_rectangle(rt::screen& s, int x, int y, int width, rt::color c)
{
    s.fill_rect(x, y, x + width - 1, y + width - 1, c);
}

static void draw_centered_cross(rt::screen& s, int x, int y, int width, rt::color c) {
    assert(width > 0);
    int my_x = x - width/2;
    int my_y = y - width/2;
    //(s, my_x, my_y, width, c);
    draw_rectangle(s, my_x, my_y, width, c);
    draw_rectangle(s, my_x + width, my_y, width, c);
    draw_rectangle(s, my_x - width, my_y, width, c);
    draw_rectangle(s, my_x, my_y + width, width, c);
    draw_rectangle(s, my_x, my_y - width, width, c);
    s.set_pixel(x, y, rt::color::RED);
}**/

int main(int argc, char *argv[]) {

    clog << "Argument number: " << argc << endl;
    for (int i = 0; i < argc; i++)
        clog << "Argument " << i << ": " << argv[i] << endl;
    clog << string(50, '_') << endl;

    rt::screen s(WIDTH, LENGTH);

    /******************************

    Sphere s1(rt::vector(300.0, 300.0, 500.0), 150.0);
    s1.set_appearance(color(0, 0, 50), 70.0, 30.0, 10.0);

    Sphere s2(rt::vector(300.0, 600.0, 500.0), 100.0);
    s2.set_appearance(color(50, 0, 0), 70.0, 30.0, 10.0);

    Plane p;
    p.set_appearance(color(0, 0, 0), 100.0, 100.0, 1.0);

    std::vector<Object *> objects;
    objects.push_back(&s1);
    objects.push_back(&s2);
    objects.push_back(&p);

    Camera camera(rt::vector(300.0, 300.0, 0.0), rt::vector(0.0, 0.0, 1.0));

    Scene scene(objects, camera, rt::vector(300.0, 800.0, 50.0));

    scene.render_scene(s);

    s.update();

    ******************************/

    Sphere s1(rt::vector(300.0, 300.0, 300.0), 100.0, rt::color::WHITE);
    s1.set_appearance(color::WHITE, 70.0, 30.0, 10.0);

    std::vector<Sphere *> objects;
    objects.push_back(&s1);

    rt::vector pos(300.0, 300.0, 0.0);
    rt::vector ori(0.0, 0.0, 1.0);

    Camera camera(pos, ori);

    //Scene scene(objects, camera, rt::vector(300.0, 800.0, 50.0));

    //scene.render_scene(s);

    s.update();

    cin.exceptions(ifstream::failbit | ifstream::badbit);

    /**for(;;) {
        int x, y;
        rt::color c = rt::color::GREEN;
        cout << "Please, enter x and y: " << flush;
        std::cin >> x >> y;
        if ((x == 0) || (y == 0)) break;
        draw_centered_cross(s, x, y, 10, c);
        s.update();
    }**/

    for (int x = 0; x < WIDTH; x++)
    {
        for (int y = 0; y < LENGTH; y++)
        {
	    rt::vector delta(-WIDTH/2+x, -LENGTH/2 + y, 0);
            rt::ray r(pos+delta, ori, rt::color::WHITE);
            double min = objects.at(0)->send(r);

            for (size_t k = 0; k < objects.size(); k++)
            {
                double dist = objects.at(k)->send(r);
                if ((dist > 0) && (dist < min || min < 0))
                    min = dist;
            }
            rt::color col = rt::color::BLACK;
            if (min > 0)
                col = rt::color::WHITE;
            s.set_pixel(x, y, col);
        }
    }
    s.update();

    // wait for the screen to be closed
    clog << "Waiting for QUIT event...";
    while(not s.wait_quit_event()) {}

    return 0;
}
